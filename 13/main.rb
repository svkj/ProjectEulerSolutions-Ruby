
def allLines(path)
    open(path) do |file|
        while line = file.gets
            yield(line)
        end
    end
end

sum = 0

allLines("data.txt") do |line|
    num = line.to_i
    sum += num
end

puts sum.to_s[0...10]
