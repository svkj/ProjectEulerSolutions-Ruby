
@singleMap = []
@singleMap[0] = ""
@singleMap[1] = "one"
@singleMap[2] = "two"
@singleMap[3] = "three"
@singleMap[4] = "four"
@singleMap[5] = "five"
@singleMap[6] = "six"
@singleMap[7] = "seven"
@singleMap[8] = "eight"
@singleMap[9] = "nine"

@tensMap = []
@tensMap[0] = ""
@tensMap[1] = "ten"
@tensMap[2] = "twenty"
@tensMap[3] = "thirty"
@tensMap[4] = "forty"
@tensMap[5] = "fifty"
@tensMap[6] = "sixty"
@tensMap[7] = "seventy"
@tensMap[8] = "eighty"
@tensMap[9] = "ninety"

@teensMap = []
@teensMap[0] = "ten"
@teensMap[1] = "eleven"
@teensMap[2] = "twelve"
@teensMap[3] = "thirteen"
@teensMap[4] = "fourteen"
@teensMap[5] = "fifteen"
@teensMap[6] = "sixteen"
@teensMap[7] = "seventeen"
@teensMap[8] = "eighteen"
@teensMap[9] = "nineteen"

def assert(condition)
    raise unless condition
end

def convertToWords(num)

    if num == 1000
        return "one thousand"
    end

    asStr = num.to_s

    assert asStr.length <= 3

    result = ""

    if asStr.length == 3
        result += @singleMap[asStr[0].to_i] + " hundred"
        asStr = asStr[1..-1]

        if asStr == "00"
            return result
        end
    end

    assert asStr.length <= 2

    if asStr.length == 2
        if result.length > 0
            result += " and "
        end

        if asStr[0] == "1"
            return result + @teensMap[asStr[1].to_i - 10]
        end

        result += @tensMap[asStr[0].to_i]
        asStr = asStr[1..-1]
    end

    if asStr[0] != "0"
        result += " " + @singleMap[asStr[0].to_i]
    end

    return result
end

def getCount(num)
    word = convertToWords(num)
    return word.gsub(/\s/) { "" }.chars.length
end

sum = 0

(1..1000).each do |x|
    sum += getCount(x)
end

puts sum
