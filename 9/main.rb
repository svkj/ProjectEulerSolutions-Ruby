
Inf = Float::INFINITY

(2..Inf).each do |b|
    (1..b).each do |a|
        c = Math.sqrt(a*a + b*b)

        if c == c.to_i
            if a + b + c == 1000
                puts "found triplet #{a}, #{b}, #{c}"
                puts "product = #{a * b * c}"
                exit
            end
        end
    end
end
