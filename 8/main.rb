
open("number.txt") do |file|
    line = file.gets

    maxProduct = 0

    chars = line.chars
    chars.length.times.each do |i|
        currentProduct = 1
        13.times do |k|
            currentProduct *= chars[i+k].to_i
        end

        if currentProduct > maxProduct
            maxProduct = currentProduct
        end
    end

    puts maxProduct.to_s
end

