
def getCollatzValue(num)
    if num == 1
        return 1
    end

    isEven = num % 2 == 0

    if isEven
        return num / 2
    else
        return 3 * num + 1
    end
end

def collatzCount(num)
    count = 1
    while num > 1 do
        num = getCollatzValue(num)
        count += 1
    end
    return count
end

maxCount = 0
currentBest = 0

1000000.downto(1).each do |x|
    count = collatzCount(x)

    if count > maxCount
        maxCount = count
        currentBest = x
        puts "#{x} produces #{count} elements"
    end
end
