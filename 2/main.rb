
def fib(x)
    return 0 if x == 0
    return 1 if x == 1

    return fib(x-1) + fib(x-2)
end

Inf = Float::INFINITY

sum = 0

(1..Inf).each do |x|
    f = fib(x)

    if f % 2 == 0
        sum += f
    end

    break if f > 4000000
end

puts sum

