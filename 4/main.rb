
def isPalindrome(s)
    (0..(s.length/2)).each do |i|
        if s[i] != s[s.length - 1 - i]
            return false
        end
    end

    return true
end

def allThreeDigitNumbers
    Enumerator.new do |enum|
        (1..9).each do |i|
            (0..9).each do |x|
                (0..9).each do |y|
                    enum.yield i * 100 + x * 10 + y
                end
            end
        end
    end
end

gen1 = allThreeDigitNumbers
gen2 = allThreeDigitNumbers

max = 0

gen1.each do |x1|
    gen2.each do |x2|
        result = x1 * x2
        if result > max and isPalindrome(result.to_s)
            max = result
        end
    end
end

puts max
