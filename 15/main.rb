
@gridSize = 20

@cache = Hash.new

def getNumPathsToEndWithCache(x, y)
    numPaths = @cache[[x,y]]

    if not numPaths
        numPaths = getNumPathsToEnd(x, y)
        @cache[[x,y]] = numPaths
    end

    return numPaths
end

def getNumPathsToEnd(x, y)
    if x == @gridSize and y == @gridSize
        return 1
    end

    sum = 0

    if x < @gridSize
        sum += getNumPathsToEndWithCache(x + 1, y)
    end

    if y < @gridSize
        sum += getNumPathsToEndWithCache(x, y + 1)
    end

    return sum
end

puts getNumPathsToEnd(0, 0)
