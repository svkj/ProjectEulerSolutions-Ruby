
def getSumOfSquares(range)
    return range.inject do |total, x|
        total += x * x
    end
end

def getSquareOfSum(range)
    sum = range.inject do |aggregate, x|
        aggregate += x
    end

    return sum * sum
end

def getDifference(range)
    return getSquareOfSum(range) - getSumOfSquares(range)
end

puts getDifference(1..100)
