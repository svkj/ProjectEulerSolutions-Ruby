
def GetPrimeFactors(x)
    (2..(x-1)).each do |i|
        if (x % i) == 0
            return  [i] + GetPrimeFactors(x / i)
        end
    end

    return [x]
end

puts GetPrimeFactors(600851475143).max

