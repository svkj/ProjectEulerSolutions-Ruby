
require 'prime'

def countFactors(x)
    count = 0
    (1..x).each do |i|
        if x % i == 0
            count += 1
        end
    end
    return count
end

Inf = Float::INFINITY

maxFoundSoFar = 0
triangleNumber = 0

(1..Inf).each do |i|
    triangleNumber += i
    numFactors = countFactors(triangleNumber)

    if numFactors > maxFoundSoFar
        maxFoundSoFar = numFactors
        puts "Found #{maxFoundSoFar} factors"
    end

    if numFactors > 500
        puts "The #{i}-th triangle number (#{triangleNumber}) has #{numFactors} factors"
        break
    end
end

