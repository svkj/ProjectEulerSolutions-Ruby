
def allLines(path)
    open(path) do |file|
        while line = file.gets
            yield(line)
        end
    end
end

$data = []

allLines("data.txt") do |line|
    $data << line.split(/\s/)
end

NumCols = $data[0].length
NumRows = $data.length

def getAt(x, y)
    if y < 0 or y >= $data.length or x < 0 or x >= $data[y].length
        return 1
    end

    return $data[y][x]
end

$maxProduct = 1

def addProduct(*args)

    product = 1

    args.each do |x|
        product *= x.to_i
    end

    if product > $maxProduct
        $maxProduct = product
    end
end

NumRows.times do |y|
    NumCols.times do |x|
        addProduct(getAt(x,y), getAt(x-1,y), getAt(x-2,y), getAt(x-3,y))
        addProduct(getAt(x,y), getAt(x+1,y), getAt(x+2,y), getAt(x+3,y))

        addProduct(getAt(x,y), getAt(x,y-1), getAt(x,y-2), getAt(x,y-3))
        addProduct(getAt(x,y), getAt(x,y+1), getAt(x,y+2), getAt(x,y+3))

        addProduct(getAt(x,y), getAt(x+1,y+1), getAt(x+2,y+2), getAt(x+3,y+3))
        addProduct(getAt(x,y), getAt(x-1,y-1), getAt(x-2,y-2), getAt(x-3,y-3))

        addProduct(getAt(x,y), getAt(x-1,y+1), getAt(x-2,y+2), getAt(x-3,y+3))
        addProduct(getAt(x,y), getAt(x+1,y-1), getAt(x+2,y-2), getAt(x+3,y-3))
    end
end

puts "Result: #{$maxProduct}"

