
Inf = Float::INFINITY
Max = 20

def isMatch(x)
    (1...Max).each do |i|
        if x % i != 0
            return false
        end
    end

    return true
end

result = nil

(Max..Inf).step(Max).each do |x|
    if isMatch(x)
        result = x
        break
    end
end

puts result
